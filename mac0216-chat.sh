#! /bin/bash
#  AO PREENCHER(MOS) ESSE CABEÇALHO COM O(S) MEU(NOSSOS) NOME(S) E
#  O(S) MEU(NOSSOS) NÚMERO(S) USP, DECLARO(AMOS) QUE SOU(MOS) O(S)
#  ÚNICO(S) AUTOR(ES) E RESPONSÁVEL(IS) POR ESSE PROGRAMA. TODAS AS
#  PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM DESENVOLVIDAS
#  E IMPLEMENTADAS POR MIM(NÓS) SEGUINDO AS INSTRUÇÕES DESSE EP E QUE
#  PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA OU PLÁGIO. DECLARO
#  TAMBÉM QUE SOU(MOS) RESPONSÁVEL(IS) POR TODAS AS CÓPIAS DESSE
#  PROGRAMA E QUE EU(NÓS) NÃO DISTRIBUÍ(MOS) OU FACILITEI(AMOS) A SUA
#  DISTRIBUIÇÃO. ESTOU(AMOS) CIENTE(S) QUE OS CASOS DE PLÁGIO E
#  DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
#  DIVULGADOS NA PÁGINA DA DISCIPLINA. ENTENDO(EMOS) QUE EPS SEM
#  ASSINATURA NÃO SERÃO CORRIGIDOS E, AINDA ASSIM, PODERÃO SER PUNIDOS
#  POR DESONESTIDADE ACADÊMICA.
#
#  Nome(s) : Joao Pedro Petrosino Costa
#  NUSP(s) : 8040538
#  Nome(s) : Maximilian Cabrajac Goritz
#  NUSP(s) : 11795418
#
#  Referências:
#		Expansao de parametros - https://wiki.bash-hackers.org/syntax/pe
#		Deletar linha com Regex - https://stackoverflow.com/questions/5410757/how-to-delete-from-a-text-file-all-lines-that-contain-a-specific-string

# guarda os usuarios logados, assim como o terminal usado por eles
LOGADOS=/tmp/logados
# guarda os usuarios criados, e suas senhas
USUARIOS=/tmp/usuarios

# arruma a entrada de barras "/" para funcoes que tem problema em le-las
escapaBarra() {
	sed 's/\//\\\//g'
}

# pega os nomes dos usuarios logados do arquivo logados
listaUsrs() {
	cut -f 1 -d ' ' < $LOGADOS
}

# so funciona para o servidor. Faz hora atual - hora que o servidor foi aberto, em segundos
tempoSrv() {
	if [ $TIPO != "servidor" ]; then
		echo ERRO
		return 1
	fi

	echo $(($(date +%s) - $HORA))
}

# chama as funcoes para fechar e abrir o servidor, reiniciando os arquivos de usuarios e logados
resetaSrv() {
	if [ $TIPO != "servidor" ]; then
		echo ERRO
		return 1
	fi
	quitaSrv
	inicializaSrv
}

# chama as respectivas funcoes para caso tenha sido chamada pelo cliente, ou servidor, e desloga o cliente caso esteja logado
quita() {
	if [ $TIPO = "servidor" ]; then
		quitaSrv
	else
		if grep -q "$(tty)\$" $LOGADOS; then
			deslogaUsr
		fi
	fi
# colca a variavel TIPO="0" para finalizar a interface cliente> ou servidor>, e finalizar o programa
	TIPO="0"
}

# cria os arquivos necessarios para o funcionamento do servidor
inicializaSrv() {
	> $USUARIOS
	> $LOGADOS
}

# remove os arquivos utilizados pelo servidor para fecha-lo
quitaSrv() {
	rm $USUARIOS $LOGADOS
}

logaUsr() {
# somente usavel pelo cliente. Verifica se o usuario ja esta logado, se o terminal ja possui um login, e se a senha esta certa
	if [ $TIPO != "cliente" ] ||
		grep -q "$(tty)\$" $LOGADOS ||
		grep -q "^$1" $LOGADOS ||
		! grep -q "^$1 $2\$" $USUARIOS; then
			echo ERRO
			return 1
	fi

# coloca o nome e o teminal do usuario no arquivo logados
	echo "$1 $(tty)" >> $LOGADOS
}

criaUsr() {
# somente para o cliente. vefirica se o usuario ja existe
	if [ $TIPO != "cliente" ] ||
		grep -q "^$1 " $USUARIOS; then
			echo ERRO
			return 1
	fi

# coloca o usuario e a senha no arquivo usuarios
	echo $@ >> $USUARIOS
}

trocaSenhaUsr() {
# somente para o clinete. Verifica se existe a combinacao de usuario e senha enviados
	if [ $TIPO != "cliente" ] ||
		! grep -q "^$1 $2\$" $USUARIOS; then
			echo ERRO
			return 1
	fi

# arruma as barras para a funcao sed
	NOME=$(escapaBarra <<< $1)
	SENHAVELHA=$(escapaBarra <<< $2)
	SENHANOVA=$(escapaBarra <<< $3)
# troca a senha velha pela nova no arquivo usuarios
	sed -i "s/^$NOME $SENHAVELHA\$/$NOME $SENHANOVA/" $USUARIOS
}

deslogaUsr() {
# somente para o cliente. Verifica se o terminal esta com um usuario logado
	if [ $TIPO != "cliente" ] ||
		! grep -q "$(tty)\$" $LOGADOS; then
			echo ERRO
			return 1
	fi

# remove,  do arquivo logados, o usuario do terminal que enviou o comando
	sed -i "/$(tty | escapaBarra)$/d" $LOGADOS
}

enviaMsg() {
# somente para o cliente. Verifica se quem esta mandando esta logado, assim como o destinatario
	if [ $TIPO != "cliente" ] ||
		! grep -q "$(tty)\$" $LOGADOS ||
		! grep -q "^$1" $LOGADOS; then
			echo ERRO
			return 1
	fi

# procura o nome do usuario enviando a mensagem, e o terminal que ira recebe-la
	NOME=$(awk -F ' ' "/ $(tty | escapaBarra)\$/{print \$1}"  $LOGADOS)
	DEST=$(awk -F ' ' "/^$(escapaBarra <<< $1) /{print \$2}" $LOGADOS)

# envia a mensagem para o terminal destinatario
	echo -n "[Mensagem do $NOME]:" > $DEST
	for i in $(seq 2 $#); do
		echo -n " ${!i}"
	done > $DEST
	echo > $DEST
# recupera a interface cliente> do destinatario
	echo -n "cliente> "> $DEST

}


# variavel que ira determinar se o programa sera rodado no modo servidor ou cliente, e se o programa sera encerrado
TIPO="0"

# inicializa o programa no modo escolhido, retornando uma mensagem de ajuda caso o programa nao tenha os argumentos corretos.
# Também guarda a hora que o servidor for iniciado
case $1 in
	cliente)TIPO=$1;;
	servidor)TIPO=$1
		inicializaSrv
		HORA=$(date +%s);;
	*)echo "formato: $0 <cliente/servidor>"
		exit 1;;
esac

# mantem a interface client> ou servidor> ate receber o comando quit
while [ $TIPO != "0" ]; do
	echo -n $TIPO\>\ 
	ENTRADA=""
	read ENTRADA

# ve a linha que foi lida ate o primeiro espaco, e chama a funcao correspondente
	case ${ENTRADA%% *} in
		list) listaUsrs;;
		time) tempoSrv;;
		reset) resetaSrv;;
		quit) quita;;
		create) criaUsr ${ENTRADA#* };;
		passwd) trocaSenhaUsr ${ENTRADA#* };;
		login) logaUsr ${ENTRADA#* };;
		logout) deslogaUsr;;
		msg) enviaMsg ${ENTRADA#* };;
		*) echo ERRO;;
	esac

done
